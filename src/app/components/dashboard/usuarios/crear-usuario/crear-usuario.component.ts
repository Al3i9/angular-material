import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {


  tiles: any[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ];

  sexo: any[] = ['Masculino', 'Femenino'];

  adicionar : boolean = true;
  titulo : string = 'Crear usuario';

  form!: FormGroup;

  constructor(private fb: FormBuilder,
              private _usuariosService: UsuarioService,
              private router: Router,
              private snackbar: MatSnackBar,
              private activateRouter: ActivatedRoute) { 
    //this.insertarUsuario();
    this.activateRouter.params.subscribe(params =>{
      const id = params['id'];
      console.log(id);
      
      this.form = this.fb.group({
        usuario: ['', Validators.required],
        nombre: ['',Validators.required],
        apellido: ['', Validators.required],
        sexo:['', Validators.required]
      });

      if(id !== 'nuevo'){
        const usuario = this._usuariosService.buscarUsuario(id);
        console.log(usuario);

        //Verifico que la longitud del objeto es cero
        if(Object.keys(usuario).length === 0){
          this.router.navigate(['/dashboard/usuario']);
        }
        
        this.form.patchValue({
          usuario: usuario.usuario,
          nombre: usuario.nombre,
          apellido: usuario.apellido,
          sexo: usuario.sexo
        });

        this.adicionar = false;
        this.titulo = 'Modificar Usuario';
      }
    })
  }

  ngOnInit(): void {
  }


  insertarUsuario(): void{
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      nombre: ['',Validators.required],
      apellido: ['', Validators.required],
      sexo:['', Validators.required]
    });
  }
//Hace que todo el formulario sea validado correctamente
  agregarUsuario():void {
    if(!this.form.valid){
      return;
    }
  
    const user : UsuarioDataI = {
      usuario: this.form.value.usuario,
      nombre: this.form.value.nombre,
      apellido: this.form.value.apellido,
      sexo: this.form.value.sexo
    }

    if(this.adicionar){
      this._usuariosService.agregarUsuario(user);

      this.router.navigate(['/dashboard/usuarios']);
  
      this.snackbar.open('El usuario fue agregado con éxito','',{
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
    }else {
      this._usuariosService.modificarUsuario(user);
      this.router.navigate(['/dashboard/usuarios']);
  
      this.snackbar.open('El usuario fue modificado con éxito','',{
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
    }
  }

  Volver(): void{
    this.router.navigate(['/dashboard/usuarios']);
  }

  
}
