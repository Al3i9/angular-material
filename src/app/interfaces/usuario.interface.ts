export interface UsuarioI {
    usuario: string;
    password: string;
}

export interface UsuarioDataI {
    usuario: string;
    nombre: string;
    apellido: string;
    sexo: string;
}